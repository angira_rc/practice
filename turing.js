var solution = function(l, w, h, m) {
    let label

    let pow4 = Math.pow(10, 4)    
    
    if (l < pow4 && w < pow4 && h < pow4 && m < 100) {
        label = 'Neither'
    } else {
        let large, big
        if (m >= 100) {
            big = true
        }

        let capacity = l * w * h
        if (l >= pow4 || w >= pow4 || h >= pow4 || capacity >= Math.pow(10, 9)) {
            console.log({ l, w, h, m })
            large = true
        }

        if (large && big) {
            label = 'Both'
        } else if (large) {
            label = 'Large'
        } else {
            label = 'Big'
        }
    }

    return label
}

let x = solution(100, 50, 70, 500)
console.log({x})