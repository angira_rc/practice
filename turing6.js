const solution = (a, b) => {
    let aPos = 0, bPos = 0, str = '', prevChar = '';

    while (bPos < b.length || aPos < a.length) {
        let aChar = a[aPos];
        let bChar = b[bPos];

        if (bChar === '*') {
            // Handle '*' as a wildcard (match 0 or more occurrences of the previous character)
            while (aPos < a.length && a[aPos] === prevChar) {
                str += a[aPos];
                aPos++;
            }

            bPos++;
        } else if (aChar === bChar || bChar === '.') {
            // Direct match or '.' as a wildcard
            str += aChar;
            aPos++;
            bPos++;
            prevChar = aChar;
        } else {
            // Mismatch
            return false;
        }
    }

    return a === str;
};

let ans = solution('abc', 'a***b*c');

console.log(ans);  // Output: true
