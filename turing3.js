var solution = k => {
    let len = k.length

    let ideal = Array.from({ length: len + 1 }, (x, i) => i)
    let missing = ideal.find(num => !k.includes(num))
    return missing
}

let x = [3, 0, 1]
const output = solution(x)
console.log(output)