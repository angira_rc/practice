'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}



/*
 * Complete the 'findMinimumCost' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function findMinimumCost(arr) {
    // Write your code here
    let cost = 0
    
    function determinePrefix (curr) {
        let prev
        for (let i in curr) {
            if (i > 0)
                if (curr[i] < prev)
                    return i
                
            prev = curr[i]
        }
        
        return 1
    }
    
    function evaluate (curr) {
        let prefix = determinePrefix(curr)
        
        let factor = curr[prefix - 1] - curr[prefix]
        console.log({ factor, prefix })
        cost += Math.abs(factor)

        curr = curr.map((num,i) => {
            if (i < prefix)
                return num + factor
                
            return num
        })
        
        return curr
        // if (!curr.all(num => num === curr[0]))
        //     evaluate(curr)
    }       
    
    evaluate(arr)
    
    return cost
}