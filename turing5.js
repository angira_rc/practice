/**
 * Task 3
 */
const fs = require("fs");
const path = require("path");
const moment = require("moment");
const { validationErrorMessages } = require("./constants");

const { products } = require('./data/task3/products.json')

/**
 * Add item to a product
 * @param {Number} productId - Product id
 * @param {Object} item - { id: 1010, expiry_date: "2050-03-30T12:57:07.846Z" }
 */
async function addItem(productId, item) {
  if (productId <= 0 || isNaN(productId))
    return validationErrorMessages.productIdValidation

  if (typeof item !== 'object' || !item?.id || !item?.expiry_date)
    return validationErrorMessages.itemValidation

  let index = products.findIndex(prod => prod.id === productId)
  if (index < 0)
    return validationErrorMessages.productNotFound

  let product = products[index]
  let exists = product.items.find(itm => itm.item_id === item?.id)
  if (exists)
    return validationErrorMessages.itemAlreadyExists

  if (new Date(item?.expiry_date) <= new Date())
    return validationErrorMessages.itemExpired

  product.items.push(item)

  product.items_left++
  product.items = product.items.sort((a, b) => a.item_id < b.item_id)

  return product
}

/**
 * TIP: Use the following code to test your implementation
 * Use different values for input parameters to test different scenarios
 */
(async () => {
  try {
    const result = await addItem(4, {
      id: 410,
      expiry_date: "2050-03-30T12:57:07.846Z",
    });
    console.log(result);
  } catch (err) {
    console.error(err);
  }
})();

module.exports = {
  addItem,
};
